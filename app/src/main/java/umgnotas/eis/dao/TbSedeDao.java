/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package umgnotas.eis.dao;

import umgnotas.eis.dto.*;
import umgnotas.eis.exceptions.*;

public interface TbSedeDao
{
	/** 
	 * Inserts a new row in the tb_sede table.
	 */
	public void insert(TbSede dto) throws TbSedeDaoException;

	/** 
	 * Returns all rows from the tb_sede table that match the criteria ''.
	 */
	public TbSede[] findAll() throws TbSedeDaoException;

	/** 
	 * Returns all rows from the tb_sede table that match the criteria 'sed_uni_codigo = :sedUniCodigo'.
	 */
	public TbSede[] findByTbUniversidad(int sedUniCodigo) throws TbSedeDaoException;

	/** 
	 * Returns all rows from the tb_sede table that match the criteria 'sed_codigo = :sedCodigo'.
	 */
	public TbSede[] findWhereSedCodigoEquals(int sedCodigo) throws TbSedeDaoException;

	/** 
	 * Returns all rows from the tb_sede table that match the criteria 'sed_nombre = :sedNombre'.
	 */
	public TbSede[] findWhereSedNombreEquals(String sedNombre) throws TbSedeDaoException;

	/** 
	 * Returns all rows from the tb_sede table that match the criteria 'sed_direccion = :sedDireccion'.
	 */
	public TbSede[] findWhereSedDireccionEquals(String sedDireccion) throws TbSedeDaoException;

	/** 
	 * Returns all rows from the tb_sede table that match the criteria 'sed_telefono = :sedTelefono'.
	 */
	public TbSede[] findWhereSedTelefonoEquals(String sedTelefono) throws TbSedeDaoException;

	/** 
	 * Returns all rows from the tb_sede table that match the criteria 'sed_fecha_apertura = :sedFechaApertura'.
	 */
	public TbSede[] findWhereSedFechaAperturaEquals(String sedFechaApertura) throws TbSedeDaoException;

	/** 
	 * Returns all rows from the tb_sede table that match the criteria 'sed_uni_codigo = :sedUniCodigo'.
	 */
	public TbSede[] findWhereSedUniCodigoEquals(int sedUniCodigo) throws TbSedeDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the tb_sede table that match the specified arbitrary SQL statement
	 */
	public TbSede[] findByDynamicSelect(String sql, Object[] sqlParams) throws TbSedeDaoException;

	/** 
	 * Returns all rows from the tb_sede table that match the specified arbitrary SQL statement
	 */
	public TbSede[] findByDynamicWhere(String sql, Object[] sqlParams) throws TbSedeDaoException;

}
