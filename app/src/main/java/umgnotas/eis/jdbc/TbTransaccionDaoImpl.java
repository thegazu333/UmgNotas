/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package umgnotas.eis.jdbc;

import umgnotas.eis.dao.*;
import umgnotas.eis.factory.*;
import umgnotas.eis.dto.*;
import umgnotas.eis.exceptions.*;
import java.sql.Connection;
import java.util.Collection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class TbTransaccionDaoImpl extends AbstractDAO implements TbTransaccionDao
{
	/** 
	 * The factory class for this DAO has two versions of the create() method - one that
takes no arguments and one that takes a Connection argument. If the Connection version
is chosen then the connection will be stored in this attribute and will be used by all
calls to this DAO, otherwise a new Connection will be allocated for each operation.
	 */
	protected java.sql.Connection userConn;

	/** 
	 * All finder methods in this class use this SELECT constant to build their queries
	 */
	protected final String SQL_SELECT = "SELECT trx_codigo, trx_fecha, trx_hora, trx_mes_anio, trx_est_carne, trx_referencia, trx_usu_codigo FROM " + getTableName() + "";

	/** 
	 * Finder methods will pass this value to the JDBC setMaxRows method
	 */
	protected int maxRows;

	/** 
	 * SQL INSERT statement for this table
	 */
	protected final String SQL_INSERT = "INSERT INTO " + getTableName() + " ( trx_codigo, trx_fecha, trx_hora, trx_mes_anio, trx_est_carne, trx_referencia, trx_usu_codigo ) VALUES ( ?, ?, ?, ?, ?, ?, ? )";

	/** 
	 * Index of column trx_codigo
	 */
	protected static final int COLUMN_TRX_CODIGO = 1;

	/** 
	 * Index of column trx_fecha
	 */
	protected static final int COLUMN_TRX_FECHA = 2;

	/** 
	 * Index of column trx_hora
	 */
	protected static final int COLUMN_TRX_HORA = 3;

	/** 
	 * Index of column trx_mes_anio
	 */
	protected static final int COLUMN_TRX_MES_ANIO = 4;

	/** 
	 * Index of column trx_est_carne
	 */
	protected static final int COLUMN_TRX_EST_CARNE = 5;

	/** 
	 * Index of column trx_referencia
	 */
	protected static final int COLUMN_TRX_REFERENCIA = 6;

	/** 
	 * Index of column trx_usu_codigo
	 */
	protected static final int COLUMN_TRX_USU_CODIGO = 7;

	/** 
	 * Number of columns
	 */
	protected static final int NUMBER_OF_COLUMNS = 7;

	/** 
	 * Inserts a new row in the tb_transaccion table.
	 */
	public void insert(TbTransaccion dto) throws TbTransaccionDaoException
	{
		long t1 = System.currentTimeMillis();
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			stmt = conn.prepareStatement( SQL_INSERT );
			int index = 1;
			if (dto.getTrxCodigo() != null) {
				stmt.setInt( index++, dto.getTrxCodigo().intValue() );
			} else {
				stmt.setNull(index++, java.sql.Types.INTEGER);
			}
		
			stmt.setString( index++, dto.getTrxFecha() );
			stmt.setString( index++, dto.getTrxHora() );
			stmt.setString( index++, dto.getTrxMesAnio() );
			stmt.setString( index++, dto.getTrxEstCarne() );
			stmt.setString( index++, dto.getTrxReferencia() );
			stmt.setString( index++, dto.getTrxUsuCodigo() );
			System.out.println( "Executing " + SQL_INSERT + " with DTO: " + dto );
			int rows = stmt.executeUpdate();
			long t2 = System.currentTimeMillis();
			System.out.println( rows + " rows affected (" + (t2-t1) + " ms)" );
			reset(dto);
		}
		catch (Exception _e) {
			_e.printStackTrace();
			throw new TbTransaccionDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria ''.
	 */
	public TbTransaccion[] findAll() throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT, null );
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria 'trx_usu_codigo = :trxUsuCodigo'.
	 */
	public TbTransaccion[] findByTbUsuario(String trxUsuCodigo) throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE trx_usu_codigo = ?", new Object[] { trxUsuCodigo } );
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria 'trx_est_carne = :trxEstCarne'.
	 */
	public TbTransaccion[] findByTbEstudiante(int trxEstCarne) throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE trx_est_carne = ?", new Object[] {  new Integer(trxEstCarne) } );
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria 'trx_codigo = :trxCodigo'.
	 */
	public TbTransaccion[] findWhereTrxCodigoEquals(int trxCodigo) throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE trx_codigo = ? ORDER BY trx_codigo", new Object[] {  new Integer(trxCodigo) } );
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria 'trx_fecha = :trxFecha'.
	 */
	public TbTransaccion[] findWhereTrxFechaEquals(String trxFecha) throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE trx_fecha = ? ORDER BY trx_fecha", new Object[] { trxFecha } );
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria 'trx_hora = :trxHora'.
	 */
	public TbTransaccion[] findWhereTrxHoraEquals(String trxHora) throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE trx_hora = ? ORDER BY trx_hora", new Object[] { trxHora } );
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria 'trx_mes_anio = :trxMesAnio'.
	 */
	public TbTransaccion[] findWhereTrxMesAnioEquals(String trxMesAnio) throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE trx_mes_anio = ? ORDER BY trx_mes_anio", new Object[] { trxMesAnio } );
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria 'trx_est_carne = :trxEstCarne'.
	 */
	public TbTransaccion[] findWhereTrxEstCarneEquals(int trxEstCarne) throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE trx_est_carne = ? ORDER BY trx_est_carne", new Object[] {  new Integer(trxEstCarne) } );
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria 'trx_referencia = :trxReferencia'.
	 */
	public TbTransaccion[] findWhereTrxReferenciaEquals(String trxReferencia) throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE trx_referencia = ? ORDER BY trx_referencia", new Object[] { trxReferencia } );
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the criteria 'trx_usu_codigo = :trxUsuCodigo'.
	 */
	public TbTransaccion[] findWhereTrxUsuCodigoEquals(String trxUsuCodigo) throws TbTransaccionDaoException
	{
		return findByDynamicSelect( SQL_SELECT + " WHERE trx_usu_codigo = ? ORDER BY trx_usu_codigo", new Object[] { trxUsuCodigo } );
	}

	/**
	 * Method 'TbTransaccionDaoImpl'
	 * 
	 */
	public TbTransaccionDaoImpl()
	{
	}

	/**
	 * Method 'TbTransaccionDaoImpl'
	 * 
	 * @param userConn
	 */
	public TbTransaccionDaoImpl(final java.sql.Connection userConn)
	{
		this.userConn = userConn;
	}

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows)
	{
		this.maxRows = maxRows;
	}

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows()
	{
		return maxRows;
	}

	/**
	 * Method 'getTableName'
	 * 
	 * @return String
	 */
	public String getTableName()
	{
		return "tb_transaccion";
	}

	/** 
	 * Fetches a single row from the result set
	 */
	protected TbTransaccion fetchSingleResult(ResultSet rs) throws SQLException
	{
		if (rs.next()) {
			TbTransaccion dto = new TbTransaccion();
			populateDto( dto, rs);
			return dto;
		} else {
			return null;
		}
		
	}

	/** 
	 * Fetches multiple rows from the result set
	 */
	protected TbTransaccion[] fetchMultiResults(ResultSet rs) throws SQLException
	{
		Collection resultList = new ArrayList();
		while (rs.next()) {
			TbTransaccion dto = new TbTransaccion();
			populateDto( dto, rs);
			resultList.add( dto );
		}
		
		TbTransaccion ret[] = new TbTransaccion[ resultList.size() ];
		resultList.toArray( ret );
		return ret;
	}

	/** 
	 * Populates a DTO with data from a ResultSet
	 */
	protected void populateDto(TbTransaccion dto, ResultSet rs) throws SQLException
	{
		dto.setTrxCodigo( new Integer( rs.getInt(COLUMN_TRX_CODIGO) ) );
		dto.setTrxFecha( rs.getString( COLUMN_TRX_FECHA ) );
		dto.setTrxHora( rs.getString( COLUMN_TRX_HORA ) );
		dto.setTrxMesAnio( rs.getString( COLUMN_TRX_MES_ANIO ) );
		dto.setTrxEstCarne( rs.getString( COLUMN_TRX_EST_CARNE ) );
		dto.setTrxReferencia( rs.getString( COLUMN_TRX_REFERENCIA ) );
		dto.setTrxUsuCodigo( rs.getString( COLUMN_TRX_USU_CODIGO ) );
	}

	/** 
	 * Resets the modified attributes in the DTO
	 */
	protected void reset(TbTransaccion dto)
	{
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the specified arbitrary SQL statement
	 */
	public TbTransaccion[] findByDynamicSelect(String sql, Object[] sqlParams) throws TbTransaccionDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = sql;
		
		
			System.out.println( "Executing " + SQL );
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			_e.printStackTrace();
			throw new TbTransaccionDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

	/** 
	 * Returns all rows from the tb_transaccion table that match the specified arbitrary SQL statement
	 */
	public TbTransaccion[] findByDynamicWhere(String sql, Object[] sqlParams) throws TbTransaccionDaoException
	{
		// declare variables
		final boolean isConnSupplied = (userConn != null);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			// get the user-specified connection or get a connection from the ResourceManager
			conn = isConnSupplied ? userConn : ResourceManager.getConnection();
		
			// construct the SQL statement
			final String SQL = SQL_SELECT + " WHERE " + sql;
		
		
			System.out.println( "Executing " + SQL );
			// prepare statement
			stmt = conn.prepareStatement( SQL );
			stmt.setMaxRows( maxRows );
		
			// bind parameters
			for (int i=0; sqlParams!=null && i<sqlParams.length; i++ ) {
				stmt.setObject( i+1, sqlParams[i] );
			}
		
		
			rs = stmt.executeQuery();
		
			// fetch the results
			return fetchMultiResults(rs);
		}
		catch (Exception _e) {
			_e.printStackTrace();
			throw new TbTransaccionDaoException( "Exception: " + _e.getMessage(), _e );
		}
		finally {
			ResourceManager.close(rs);
			ResourceManager.close(stmt);
			if (!isConnSupplied) {
				ResourceManager.close(conn);
			}
		
		}
		
	}

}
